import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  TouchableOpacity,
  ScrollView,
  WebView,
  SegmentedControlIOS,
} from 'react-native';

import NavigationBar from 'react-native-navbar';
import horaire from './src/horaire';
import infos from './src/infos';
import notes from './src/notes';
import fichiers from './src/fichiers';
import icons from './src/icons';

var csud = React.createClass({
  getInitialState() {
    return {
      selectedTab: 'tab_horaire',
    };
  },

  _navBar(navbar) {
    return (
      <NavigationBar
        tintColor='rgb(247,247,247)'
        style={styles.navbar}
        title={{title: navbar.title}}
        leftButton={{title: navbar.leftButton}}
        rightButton={{title: navbar.rightButton}} />
    );
  },

  _renderContent(navBarInfos, rendered_JSX) {
    if (typeof(navBarInfos.leftButton) === 'undefined') {
      navBarInfos.leftButton = '';
    }
    if (typeof(navBarInfos.rightButton) === 'undefined') {
      navBarInfos.rightButton = '';
    }
    return (
      <View style={{flex:1}}>
        {this._navBar(navBarInfos)}
        <View style={{flex:1}}>
          <ScrollView>
            <View style={{flex:1}}>
              {rendered_JSX}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  },

  render() {
    return (
      <TabBarIOS
        unselectedTintColor="rgb(123,132,132)"
        tintColor="rgb(0,132,255)"
        barTintColor="rgb(247,247,247)">
        <TabBarIOS.Item
          title="Horaire"
          icon={{uri: icons.calendar, scale: 1.8}}
          selected={this.state.selectedTab === 'tab_horaire'}
          onPress={() => { this.setState({ selectedTab: 'tab_horaire' })}}>
          {this._renderContent({title: 'Horaire - 3GY1', rightButton: 'Modifier'}, horaire())}
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Infos"
          icon={{uri: icons.info, scale: 1.8}}
          selected={this.state.selectedTab === 'tab_infos'}
          style={{backgroundColor: 'rgb(233,234,237)'}}
          onPress={() => { this.setState({ selectedTab: 'tab_infos' })}}>
          {this._renderContent({title: 'Infos'}, infos())}
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Fichiers"
          icon={{uri: icons.folder, scale: 1.8}}
          selected={this.state.selectedTab === 'tab_fichiers'}
          onPress={() => { this.setState({ selectedTab: 'tab_fichiers' })}}>
          {fichiers()}
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Notes"
          icon={{uri: icons.note, scale: 1.8}}
          selected={this.state.selectedTab === 'tab_notes'}
          onPress={() => { this.setState({ selectedTab: 'tab_notes' })}}>
          {notes()}
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Paramètres"
          icon={{uri: icons.settings, scale: 3.6}}
          selected={this.state.selectedTab === 'tab_chat'}
          onPress={() => { this.setState({ selectedTab: 'tab_chat' })}}>
          {this._renderContent({title:'Paramètres'}, <Text>Settings</Text>)}
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  },

});

var styles = StyleSheet.create({
  navbar: {
    borderBottomColor: 'rgb(206,206,206)',
    borderBottomWidth: 1,
  },
});

AppRegistry.registerComponent('csud', () => csud);