import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  SegmentedControlIOS,
  Navigator,
  WebView,
} from 'react-native';

import base64 from 'base-64';
import CookieManager from 'react-native-cookies';
import moment from 'moment';
import fileOpener from 'react-native-file-opener';
import RNFS from 'react-native-fs';
var DomParser = require('react-native-html-parser').DOMParser;

var Fichiers = React.createClass({
  renderScene: function(route, navigator) {
    return <route.component navigator={navigator} {...route.passProps} />
  },
   
  configureScene: function(route, routeStack){
    return Navigator.SceneConfigs.HorizontalSwipeJump;
  },

  _navBar: function(navbar) {
    return (
      <NavigationBar
        tintColor='rgb(247,247,247)'
        style={styles.navbar}
        title={{title: navbar.title}}
        leftButton={{title: navbar.leftButton}}
        rightButton={{title: navbar.rightButton}} 
      />
    );
  },
  
  render() {
    return (
      <Navigator
        configureScene={this.configureScene}
        style={{flex:1}}
        initialRoute={{component: fileExplorer}}
        renderScene={this.renderScene} 
        navigationBar={
          <Navigator.NavigationBar 
            style={ styles.navBar } 
            routeMapper={NavigationBarRouteMapper}/>
        } 
      />
    );
  },
});

var fileViewer = React.createClass({

  render() {
    var filePath = this.props.filePath;
    return (
      <View style={{flex:1, marginTop: 65}}>
        <WebView
          ref={'webview'}
          source={{
            uri: 'https://mail.studentfr.ch/' + filePath,
          }}
          scalesPageToFit={true}
        />
      </View>
    );
  },
});

var fileExplorer = React.createClass({

  _navigate(activePath, path, folderContent, types) {
		this.props.navigator.push({
    component: fileExplorer,
    passProps: {
      path: path,
      activePath: activePath,
      folderContent: folderContent,
      types: types,
    }});
  },

  _navigateToFile(activePath, file, mimeType) {
    this.props.navigator.push({
    component: fileViewer,
    passProps: {
      activePath: activePath,
      filePath: file,
      types: 'file',
      mimeType: mimeType,
    }});
  },

  getFoldersList: function(path, callback) {
  	var headers = new Headers({
        "Authorization": "Basic ZnJpbmdlbGlzQHN0dWRlbnRmci5jaDpQYzRfZWt5YTM=",
        "Depth": "1",
    });
		fetch("https://mail.studentfr.ch/" + path,{
			method: 'PROPFIND', 
			headers: headers,
		})
		  .then((response) => response.text())
      .then((responseText) => {
      	var xml = new DomParser().parseFromString(responseText,'text/xml');
      	var files = xml.getElementsByTagName('D:displayname');
      	var folderContent = [];
      	var types = [];
      	// files[0].textContent est le nom du dossier actuel
      	for (var i = 1; i < files.length; i++) {
      		folderContent[i-1] = files[i].textContent;
      		types[i-1] = files[i].parentNode.getElementsByTagName('D:getcontenttype')[0].textContent;
      		if (types[i-1] === '') { types[i-1] = 'directory'; }
      	}
      	callback(folderContent, types);
      })
		  .done();
	},

  webViewLogin(callback) {
    var headers = new Headers({
        'Authorization': 'Basic ZnJpbmdlbGlzQHN0dWRlbnRmci5jaDpQYzRfZWt5YTM=',
    },);
    fetch('https://mail.studentfr.ch/fri-doc/?logout',{
      method: 'HEAD',
      headers: headers,
    })
      .then((res) => {
        fetch('https://mail.studentfr.ch/fri-doc/',{
          method: 'HEAD', 
          headers: headers,
        })
          .then((res) => {
            cookie = 
              JSON.parse(
                JSON.stringify(res)
                  .replace('set-cookie', 'setCookie')
              )
            .headers.map.setCookie[0]
            CookieManager.set({
              name: 'Navajo',
              value: cookie.split(';')[0].replace('Navajo=', ''),
              domain: 'mail.studentfr.ch',
              origin: 'mail.studentfr.ch',
              expiration: moment().add(1, 'days').format("YYYY-MM-DDTHH:mm:ss.sssZ"),
              path: '/',
              version: '1',
            }, (err, res) => {
              callback();
            });
          });
      });
  },

	onPressHandler(activePath, file, type) {
    if (type === 'directory') {
      if (activePath === 'fri-doc/' && file === 'Personnel') {
        return () => {
          this.getFoldersList('fri-doc/Personnel/CSUD/FringeliS/', (newFolderContent, types) => {
            this._navigate('Personnel', 'fri-doc/Personnel/CSUD/FringeliS/', newFolderContent, types);
          });
        }
      }
      return () => {
  			this.getFoldersList(activePath + file + '/', (newFolderContent, types) => {
  	  		this._navigate(file, activePath + file + '/', newFolderContent, types);
  	  	});
  	  }
    } else if (
      type === 'application/pdf' || 
      type === 'text/plain' || 
      type === 'text/html' ||
      type === 'image/png' ||
      type === 'image/jpeg' ||
      /openxmlformats-officedocument/.test(type)) {
        return () => { 
          this.webViewLogin(() => {
           this._navigateToFile(file, activePath + file, type);
          });
        }
    } else {
      return () => {
        console.error(type);
      }
    }
	},

  _renderContent(activePath, folderContent, types) {
		var renderedContent = [];
		for (var i = 0; i < folderContent.length; i++) {
			renderedContent[i] = (
				<View key={i} style={{flex:1}}>
	      	<TouchableHighlight
	      		style={styles.fileContainer}
	      	  underlayColor={'rgb(217,217,217)'}
	      	  onPress={this.onPressHandler(activePath, folderContent[i], types[i])}>
	          <Text style={styles.fileText}>{folderContent[i]}</Text>
	        </TouchableHighlight>
	      </View>
			);
		}
		return renderedContent;
  },

  render() {
  	var activePath = this.props.path;
  	var folderContent = this.props.folderContent;
  	var types = this.props.types;
  	var containerStyle = styles.container;
  	if (typeof(activePath) === 'undefined') {activePath = 'fri-doc/'} 
		if (typeof(folderContent) === 'undefined') {folderContent = ['Commun', 'Personnel']}
		if (typeof(types) === 'undefined') {types = ['directory', 'directory']}
		if (activePath === 'fri-doc/') {containerStyle = styles.firstContainer}
    return (
      <View style={containerStyle}>
      	<ScrollView>
      		<View style={styles.files}>
      			{this._renderContent(activePath, folderContent, types)}
      		</View>
	      </ScrollView>
      </View>
    );
  },
});

function openFile(activePath, filePath, mimeType) {
  var headers = new Headers({
    'Authorization': 'Basic ZnJpbmdlbGlzQHN0dWRlbnRmci5jaDpQYzRfZWt5YTM=',
  });
  savePath = RNFS.DocumentDirectoryPath;
  var downloader = RNFS.downloadFile({
    fromUrl: 'https://mail.studentfr.ch/' + filePath,
    toFile: savePath + '/' + activePath,
    progressDivider: 100,
    headers: headers
  });
  downloader.promise.then(() => {
    fileOpener.open(
      savePath + '/' + activePath, mimeType
    ).then(() => {
      // success
      return;
    },(e) => {
      // error
      console.error(e);
    });
  });
}

var NavigationBarRouteMapper = {
  LeftButton(route, navigator, index, navState) {
    if(index > 0) {
      return (
        <TouchableHighlight
        	style={styles.navBarButton}
          underlayColor="transparent"
          onPress={() => { if (index > 0) { navigator.pop() } }}>
          <Text style={ styles.navBarButtonText }>Back</Text>
        </TouchableHighlight>
    )} 
    else { return }
  },
  RightButton(route, navigator, index, navState) {    
    try {
      var rightButtonText = route.passProps.types;
      if (rightButtonText !== 'file') {
        rightButtonText = '';
      } else {
        rightButtonText = 'ouvrir'
      }
    }
    catch(e) {var rightButtonText = ''}
    return (
      <TouchableHighlight
        style={styles.navBarButton}
        underlayColor="transparent"
        onPress={() => { 
          if(rightButtonText === 'ouvrir') {
            openFile(route.passProps.activePath, route.passProps.filePath, route.passProps.mimeType);
          }
        }}>
        <Text style={ styles.navBarButtonText }>{rightButtonText}</Text>
      </TouchableHighlight>
    )
  },
  Title(route, navigator, index, navState) {
    try {var title = route.passProps.activePath;}
    catch(e) {var title = 'fri-doc';}
    return (
    	<View style={styles.titleContainer}>
    		<Text style={styles.title}>{title}</Text>
    	</View>
    );
  }
};

var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 64,
    backgroundColor: 'rgb(240,239,245)',
  },
  firstContainer: {
  	flex: 1,
    marginTop: 44,
    backgroundColor: 'rgb(240,239,245)',
  },
  files: {
  	marginBottom: 51,
  },
  title: {
  	fontSize: 17,
  	letterSpacing: 0.5,
  	color: '#333',
  	fontWeight: '500',
  },
  titleContainer: {
  	flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 43
  },
  navBar: {
    height: 64,
    backgroundColor: 'rgb(247,247,247)',
    borderBottomColor: 'rgb(206,206,206)',
    borderBottomWidth: 1,
  },
  navBarButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 43
  },
  navBarButtonText: {
    fontSize: 17,
    letterSpacing: 0.5,
    color: '#0076FF',
    marginLeft: 8,
    marginRight: 8,
  },
  fileContainer: {
  	backgroundColor: 'white',
  	padding: 15,
  	borderBottomWidth: 1,
  	borderBottomColor: 'rgb(226,226,228)',
  	height: 51,
  },
  fileText: {
  	fontSize: 17,
  	fontWeight: '500',
  },
});

module.exports = () => <Fichiers/>