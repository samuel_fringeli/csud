import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  SegmentedControlIOS,
  Navigator,
} from 'react-native';

import DatePicker from 'react-native-datepicker';
import moment from 'moment';
var frLocale = require('moment/locale/fr');
moment.updateLocale('fr', frLocale);

var Notes = React.createClass({
  renderScene: function(route, navigator) {
    return <route.component navigator={navigator} {...route.passProps} />
  },
   
  configureScene: function(route, routeStack){
    return Navigator.SceneConfigs.HorizontalSwipeJump;
  },

  _navBar: function(navbar) {
    return (
      <NavigationBar
        tintColor='rgb(247,247,247)'
        style={styles.navbar}
        title={{title: navbar.title}}
        leftButton={{title: navbar.leftButton}}
        rightButton={{title: navbar.rightButton}} 
      />
    );
  },
  
  render() {
    return (
      <Navigator
        configureScene={this.configureScene}
        style={{flex:1}}
        initialRoute={{component: Branches}}
        renderScene={this.renderScene} 
        navigationBar={
          <Navigator.NavigationBar 
            style={ styles.navBar } 
            routeMapper={NavigationBarRouteMapper}/>
        } 
      />
    );
  },
});

var Branches = React.createClass({
	_navigate(branch) {
		this.props.navigator.push({
    component: Details,
    passProps: {
      branch: branch,
    }});
  },
  onPressHandler(branch) {
  	return () => { 
  		this._navigate(branch) 
  	}
  },
	render() {
		var branches_final = [];
		var branches = ['Maths', 'Français', 'Allemand', 'Latin', 'OS Musique', 'Histoire'];
		for (var i = 0; i < branches.length; i++) {
			branches_final[i] = (
				<TouchableHighlight 
					key={i} 
					underlayColor={'rgb(60,60,60)'}
					onPress={ this.onPressHandler(branches[i]) }>
					<View style={styles.branchContainer}>
						<View style={styles.branchNameItem}>
							<Text style={styles.branchNameText}>{branches[i]}</Text>
						</View>
						<View style={styles.branchNoteItem}>
							<Text style={styles.branchNoteText}>
								6.0
							</Text>
						</View>
					</View>
				</TouchableHighlight>
			);
		}
		return (
			<View style={styles.container}>
				<ScrollView>
					{branches_final}
				</ScrollView>
			</View>
		)
	}
});

var Details = React.createClass({
	getInitialState() {
	  return {
	    date: '',
	    coefText: '',
	    noteText: '',
	  };
	},

	render() {
		var details1_final = [];
		var details2_final = [];
		var detailsTexts = ['Moyenne 1/2', 'Moyenne 1/100', ]//'Note minimale', 'Note maximale'];
		var detailsNotes = ['6.0', '6.00']
		for (var i = 0; i < detailsTexts.length; i++) {
			details1_final[i] = (
				<View key={i} style={styles.branchContainer}>
					<View style={styles.branchNameItem}>
						<Text style={styles.branchNameText}>{detailsTexts[i]}</Text>
					</View>
					<View style={styles.detailsNoteBorder}>
					</View>
					<View style={styles.detailsNoteItem}>
						<Text style={styles.detailsNoteText}>
							{detailsNotes[i]}
						</Text>
					</View>
				</View>
			)
		}
		for (var i = 0; i < 50; i++) {
			details2_final[i] = (
				<View key={i} style={styles.branchContainer}>
					<View style={styles.branchNameItem}>
						<Text style={styles.detailsDateText}>26 août 2016</Text>
					</View>
					<View style={styles.detailsNoteBorder}>
					</View>
					<View style={styles.detailsNoteItem}>
						<Text style={styles.detailsNoteText}>
							1.00
						</Text>
					</View>
					<View style={styles.detailsNoteBorder}>
					</View>
					<View style={styles.detailsNoteItem}>
						<Text style={styles.detailsNoteText}>
							6.00
						</Text>
					</View>
				</View>
			)
		}
		return (
			<View style={styles.detailsContainer}>
				<ScrollView>
					<View style={{marginBottom: 40}}>
						{details1_final}
					</View>
					<View style={styles.branchContainer}>
						<View style={[
							styles.branchNameItem, 
							styles.detailsDateTextItem, 
							styles.blueBackground, 
							styles.borderTop
						]}>
							<Text style={styles.detailsHeadDateText}>
								Date
							</Text>
						</View>
						<View style={styles.detailsNoteBorder}>
						</View>
						<View style={[styles.blueBackground, styles.borderTop]}>
							<View style={styles.detailsNoteItem}>
								<Text style={styles.detailsNoteCoefText}>
									Coef.
								</Text>
							</View>
						</View>
						<View style={styles.detailsNoteBorder}>
						</View>
						<View style={[styles.blueBackground, styles.borderTop]}>
							<View style={styles.detailsNoteItem}>
								<Text style={styles.detailsNoteCoefText}>
									Note
								</Text>
							</View>
						</View>
					</View>
					<View style={{marginBottom: 50}}>
						<View style={[styles.branchContainer, styles.addNoteButton]}>
							<TouchableHighlight
				        style={styles.navBarButton}
				        underlayColor="transparent"
				        onPress={() => { alert('bonjour') }}>
				        <Text style={ styles.navBarButtonText }>Ajouter une note</Text>
				      </TouchableHighlight>
						</View>
						<View style={[styles.branchContainer, {alignItems: 'center', backgroundColor: 'rgb(240,255,255)'}]}>
							<View style={{flex:1}}>
								<DatePicker
								  date={this.state.date}
								  format="LL"
								  confirmBtnText="Confirmer"
								  cancelBtnText="Annuler"
								  showIcon={false}
								  onDateChange={(date) => {this.setState({date: date})}}
								  customStyles={{
								    dateInput: {
								      flex: 1,
								      borderWidth: 0,
								      height: 51,
								    },
								    dateText: {
								    	fontSize: 20,
								    	color: 'black',
								    },
								  }}
								/>
							</View>
							<View style={styles.detailsNoteBorder}>
							</View>
							<View style={styles.detailsNoteItem}>
								<View>
									<TextInput
										style={styles.coefNoteInput}
										keyboardType="numeric"
										placeholder="coef."
										onChangeText={(text) => {this.setState({coefText: text})}}
										>
									</TextInput>
								</View>
							</View>
							<View style={styles.detailsNoteBorder}>
							</View>
							<View style={styles.detailsNoteItem}>
								<View>
									<TextInput
										style={styles.coefNoteInput}
										keyboardType="numeric"
										placeholder="note"
										onChangeText={(text) => {this.setState({noteText: text})}}
										>
									</TextInput>
								</View>
							</View>
						</View>
						{details2_final}
					</View>
				</ScrollView>
			</View>
		)
	},
});

var NavigationBarRouteMapper = {
  LeftButton(route, navigator, index, navState) {
    if (index > 0) {
      return (
        <TouchableHighlight
        	style={styles.navBarButton}
          underlayColor="transparent"
          onPress={() => { if (index > 0) { navigator.pop() } }}>
          <Text style={ styles.navBarButtonText }>Back</Text>
        </TouchableHighlight>
    )} 
    else { return }
  },
  RightButton(route, navigator, index, navState) {    
    if (index > 0) {
	    return (
	      <TouchableHighlight
	        style={styles.navBarButton}
	        underlayColor="transparent"
	        onPress={() => { alert('bonjour') }}>
	        <Text style={ styles.navBarButtonText }>modifier</Text>
	      </TouchableHighlight>
	    )
  	}
  },
  Title(route, navigator, index, navState) {
  	try {var title = route.passProps.branch;}
    catch(e) {var title = 'Branches';}
    return (
    	<View style={styles.titleContainer}>
  			<Text style={styles.title}>{title}</Text>
  		</View>
  	)
  }
};

var styles = StyleSheet.create({
	container: {
    flex: 1,
    marginTop: 44,
    backgroundColor: 'rgb(240,239,245)',
  },
  detailsContainer: {
  	flex: 1,
    marginTop: 64,
    backgroundColor: 'rgb(240,239,245)',
  },
	title: {
  	fontSize: 17,
  	letterSpacing: 0.5,
  	color: '#333',
  	fontWeight: '500',
  },
  titleContainer: {
  	flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 43
  },
  navBar: {
    height: 64,
    backgroundColor: 'rgb(247,247,247)',
    borderBottomColor: 'rgb(206,206,206)',
    borderBottomWidth: 1,
  },
  navBarButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 43
  },
  navBarButtonText: {
    fontSize: 17,
    letterSpacing: 0.5,
    color: '#0076FF',
    marginLeft: 8,
    marginRight: 8,
  },
  branchNameItem: {
  	justifyContent: 'center',
  	flex: 1,
  },
  branchNameText: {
  	fontSize: 20,
		marginLeft: 10,
		fontWeight: '500',
  },
  detailsHeadDateText: {
  	fontSize: 20,
		fontWeight: '500',
  },
  detailsDateText: {
  	fontSize: 20,
		marginLeft: 10,
  },
  detailsDateTextItem: {
  	alignItems: 'center',
  },
  branchNoteItem: {
  	justifyContent: 'center',
  	width: 60,
  },
  branchNoteText: {
  	fontSize: 20,
  	marginLeft: 8,
  },
  branchContainer: {
  	borderBottomColor: 'rgb(206,206,206)',
    borderBottomWidth: 1,
    height: 51,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  addNoteButton: {
  	justifyContent: 'center',
  	alignItems: 'center',
  },
  detailsNoteItem: {
  	marginTop: 12,
  	marginBottom: 13,
  	width: 70,
  	alignItems: 'center',
  },
  detailsNoteText: {
  	fontSize: 20,
  },
  detailsNoteBorder: {
  	backgroundColor: 'rgb(206,206,206)',
  	width: 1,
  	height: 51,
  },
  detailsNoteCoefText: {
  	fontSize: 20,
		fontWeight: '500',
  },
  blueBackground: {
  	backgroundColor: 'rgb(245,245,245)',
  },
  borderTop: {
  	borderTopColor: 'rgb(206,206,206)',
    borderTopWidth: 1,
  },
  coefNoteInput: {
  	height: 30,
  	width: 70,
  	marginLeft: 18,
  },
});

module.exports = () => <Notes/>