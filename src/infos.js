import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import ls from './lipsum';

var Infos = React.createClass({
	render() {
		var infos = [];
		var nombre_infos = 10
		for (var i = 0; i < 10; i++) {
			infos[i] = (
				<View key={i} style={styles.block}>
					<View style={styles.title}><Text style={styles.titleText}>Info {i+1}</Text></View>
					<View style={styles.content}><Text>{ls}</Text></View>
				</View>
			);
		}
		return (
			<View style={styles.mainContainer}>
				{infos}
			</View>
		);
	}
});

var styles = StyleSheet.create({
	mainContainer: {
		marginTop: -20
	},
	block: {
		flex: 1,
		marginTop: 10,
		marginLeft: 10,
		marginRight: 10,
	},
	title: {
		paddingLeft: 5,
		paddingTop: 3,
		paddingBottom: 22,
		backgroundColor: 'rgb(246,247,248)',
		height: 20,
		borderBottomColor: 'rgb(225,225,225)',
		borderBottomWidth: 1,
	},
	titleText: {
		fontWeight: 'bold',
	},
	content: {
		backgroundColor: 'white',
		padding: 5
	},
});

module.exports = () => <Infos/>