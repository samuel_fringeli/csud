import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  TouchableOpacity,
  ScrollView,
  SegmentedControlIOS,
} from 'react-native';

var Horaire = React.createClass({
	getInitialState: function() {
		return {
			selectedIndex: 2,
		}
	},

	horaire_final: function(jour) {
		var heures = [
			'8h10','8h55',
			'9h00','9h45',
			'10h05','10h50',
			'10h55','11h40',
			'11h45','12h30',
			'12h35','13h20',
			'13h25','14h10',
			'14h15','15h00',
			'15h05','15h50',
			'15h55','16h40',
			'16h45','17h30'
		];
		var horaire = [
			['Allemand','WANP/204','Philosophie','MOUL/106','Musique','MAIB/125','Musique','MAIB/125','Histoire','FRAP/001','','','Maths','SCYJ/307','Français','CASS/204','Latin','SUDB/109','','','',''],
			['Français','CASS/204','Maths','SCYJ/114','','','Physique','OBEG/215','Latin','SUDB/109','','','Sport','GATL/SO','Sport','GATL/SO','Sport','GATL/SO','Sport','GATL/SO','Sport','GATL/SO'],
			['Allemand','WANP/109','Allemand','WANP/109','Maths','SCYJ/303','Histoire','FRAP/208','','','Physique','OBEG/217','Français','CASS/309','Français','CASS/309','Informatique','DONC/102','Informatique','DONC/102','',''],
			['Allemand','WANP/105','Latin','SUDB/109','','','Maths','SCYJ/205','','','','','','','','','','','','','',''],
			['Français','CASS/304','Allemand','WANP/107','Musique','MAIB/125','Musique','MAIB/125','PC','CASS/208','','','Philosophie','MOUL/108','Philosophie','MOUL/108','Anglais','MILE/104','Anglais','MILE/104','',''],
		];
		var horaire_final = [];
		for (var i = 0; i < heures.length; i+=2) {
			horaire_final[i] = (
				<View key={i} style={styles.timetable_container}>
					<View style={styles.hourBox}>
						<View style={styles.hourItem}>
							<Text style={styles.hourText}>{heures[i]}</Text>
							<Text style={styles.hourText}>{heures[i+1]}</Text>
						</View>
					</View>
					<View style={styles.branch}>
						<Text style={styles.branchText}>{horaire[jour][i]}</Text>
					</View>
					<View style={styles.teacher_room}>
						<Text style={styles.teacher_roomText}>{horaire[jour][i+1].split('/')[0]}</Text>
						<Text style={styles.teacher_roomText}>{horaire[jour][i+1].split('/')[1]}</Text>
					</View>
				</View>
			);
		}
		return horaire_final;
	},

	render: function() {
		var jours_semaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'];
		return (
			<View>
				<View>
					<SegmentedControlIOS 
						style={styles.segmentedControl}
						values={jours_semaine}
						selectedIndex={this.state.selectedIndex}
						onChange={(event) => {
							this.setState({
								selectedIndex: event.nativeEvent.selectedSegmentIndex,
							});
						}}
					/>
				</View>
				<View style={styles.mainContainer}>
					{this.horaire_final(this.state.selectedIndex)}
				</View>
			</View>
		);
	},
});

var styles = StyleSheet.create({
	hourText: {
		fontSize: 20,
		marginLeft: 10,
		color: 'rgb(15,24,122)',
	},
	hourItem: {
		height: 60,
		paddingTop: 5,
		paddingBottom: 5,
	},
	hourBox: {
		width: 80,
	},
	branch: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
	},
	branchText: {
		fontSize: 28,
	},
	teacher_room: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 80,
	},
	teacher_roomText: {
		fontSize: 20,
		color: 'grey',
	},
	timetable_container: {
		height: 60,
		borderBottomColor: 'rgb(206,206,206)',
    borderBottomWidth: 1,
    flexDirection: 'row',
	},
	segmentedControl: {
		marginTop: -15,
		marginBottom: 5,
		marginLeft: 5,
		marginRight: 5,
	},
	mainContainer: {
	}
});

module.exports = () => <Horaire/>